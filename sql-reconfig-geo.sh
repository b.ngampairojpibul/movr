#!/bin/bash

set -ex

python3 -u ./loadmovr.py \
    --url 'cockroachdb://root@XXXXXXXXX:5432/movr?sslmode=verify-ca&sslcert=certs/client.root.crt&sslkey=certs/client.root.key&sslrootcert=certs/ca.crt' \
    --echo-sql \
     partition --region-city-pair asia_east:kowloon --region-city-pair asia_east:central \
     --region-city-pair asia_southeast:singapore  --region-city-pair asia_southeast:tampines --region-city-pair asia_southeast:hougang \
     --region-zone-pair asia_east:asia-east2 --region-zone-pair asia_southeast:asia-southeast1
