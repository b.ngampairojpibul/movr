#!/bin/bash

set -ex

python3 -u ./loadmovr.py \
    --url 'cockroachdb://root@XXXXXXXXX:5432/movr?sslmode=verify-ca&sslcert=certs/client.root.crt&sslkey=certs/client.root.key&sslrootcert=certs/ca.crt' \
    --log-level error --num-threads 30 run --multi-region --city "singapore"
