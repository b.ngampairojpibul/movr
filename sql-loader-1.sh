#!/bin/bash

set -ex

python3 -u ./loadmovr.py \
    --url 'cockroachdb://root@XXXXXXXXX:5432/movr?sslmode=verify-ca&sslcert=certs/client.root.crt&sslkey=certs/client.root.key&sslrootcert=certs/ca.crt' \
    --echo-sql \
    load --num-users 100 --num-rides 100 --num-vehicles 10 \
    --multi-region \
    --city kowloon --city central \
    --city singapore  --city hougang  --city tampines
